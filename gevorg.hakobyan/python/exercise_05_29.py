for rate in range(5, 11):
    principal = 24.00

    print '{:7}'.format(" "), 'Rate: ', rate, ' %\n'
    print '{:<10}'.format("\nYear"), '{:>27}'.format('Amount on deposit\n')

    percentage = 1.0 + rate * 0.01
    for year in range(1, 391):
        principal *= percentage
        print year, '{0:>30.2f}'.format(principal), " $"

