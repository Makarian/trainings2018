#include <iostream>

int
main()
{
    std::cout << "1 2 3 4" << std::endl; /// a)
    std::cout << "1 " << "2 " << "3 " << "4" << std::endl; /// b)
    std::cout << "1 "; /// c)
    std::cout << "2 ";
    std::cout << "3 ";
    std::cout << "4" << std::endl;
    return 0;
}
