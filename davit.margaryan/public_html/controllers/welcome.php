<?php

namespace Controllers;
use Libs\Controller;

///////////////////////////////////////////////////////////////////
// 
// class Welcome extends Controller
//
// Control Welcome page.
//
// public function __construct()
// public function index()
//
///////////////////////////////////////////////////////////////////

class Welcome extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        try {
            $this->setData('news', $this->useModel('News')->get());
            self::view()->load('welcome/index', $this->getData());
        } catch (Exception $e) {
            die ($e->getMessage());
        }
    }
}
?>