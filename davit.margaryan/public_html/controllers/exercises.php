<?php

namespace Controllers;
use Libs\Controller;

///////////////////////////////////////////////////////////////////
// 
// class Exercises extends Controller
//
// Control Exercises page.
//
// public function __construct()
// public function index()
// public function exercise($id = NULL)
//
///////////////////////////////////////////////////////////////////

class Exercises extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        try {
            $this->setData('exercises', $this->useModel('Exercise')->get());
            self::view()->load('exercises/index', $this->getData());
        } catch (Exception $e) {
            die ($e->getMessage());
        }
    }

    public function exercise($id = NULL)
    {
        // TODO: Do the $_POST values validation from here
        $this->setData('diamond', $this->createDiamond($_POST['height']));
        self::view()->load('exercises/exercise_05_'.$id, $this->getData());
    }

    /**
     * TODO: This function must come from service provider
     * Print diamond
     *
     * @param  int    $height
     * @return string $diamond
     */
    private function createDiamond($height)
    {
        if (empty($height) || $height <= 0) {
            return;
        }
        $diamond = "\n";
        $range = (int)($height / 2);
        for ($i = 0; $i < $height; ++$i) {
            $spaces = ($range - $i > 0) ? $range - $i : $i - $range;
            for ($j = 1; $j <= $height - $spaces; ++$j) {
                $diamond .= ($j <= $spaces) ? " " : "*";     
            }
            $diamond .= "\n";
        }
        return $diamond;
    }
}
?>
