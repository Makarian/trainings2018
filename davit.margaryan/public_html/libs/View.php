<?php

namespace Libs;

///////////////////////////////////////////////////////////////////
// 
// CLASS View
//
// Controls all views, includes the nessary php view files
// from controller
//
// protected $data_ - array
// 
//
///////////////////////////////////////////////////////////////////

class View
{
    private $data_;
    private static $view = NULL;

    private function __construct() {}
    private function __clone() {}

    public static function Create()
    {
        if (is_null(self::$view)) {
            self::$view = new View();
        }
        return self::$view;
    }

    public function get($key = NULL)
    {
        if (is_null($key)) {
            return $this->data_;
        } elseif (array_key_exists($key, $this->data_)) {
            return $this->data_[$key];
        }
        return NULL;
    }

    private function setData(array $value)
    {
        $this->data_ = $value;
    }

    public function load($name, array $value = [])
    {
        $this->setData($value);
        include_once 'views/header.php';
        $file = 'views/'.$name.'.php';
        if (file_exists($file)) {
            include_once $file;
        } else {
            include_once 'views/error/index.php';
        }
        include_once 'views/footer.php';
    }
}
?>