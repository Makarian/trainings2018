<?php

namespace Libs;
use \Exception;

///////////////////////////////////////////////////////////////////
// 
// ABSTRACT CLASS Controller
//
// Controls all pages, input and output information.
// Combines all libraries, service providers, processes and jobs.
//
// protected $data_ - array
// 
//
///////////////////////////////////////////////////////////////////

abstract class Controller
{
    private $data_   = [];
    private $models_ = [];
    private static $view_;

    public function __construct()
    {
        try {
            $this->data_['menu'] = $this->useModel('Menu')->get();
            $this->data_['logo'] = $this->useModel('Logo')->get();
        } catch (Exception $e) {
            die ($e->getMessage());
        }
        self::viewInit();
    }

    public function __call($method, $args)
    {
        throw new Exception("Error");
    }

    protected function useModel($name)
    {
        if (array_key_exists($name, $this->models_)) {
            return $this->models_[$name];
        }
        $model = "Models\\$name";
        if (class_exists($model)) {
            $this->models_[$name] = new $model();
            return $this->models_[$name];
        }
        throw new Exception("The model $name does not exists!");
    }

    protected function getData()
    {
        return $this->data_;
    }

    protected function setData($key, $value)
    {
        $this->data_[$key] = $value;
    }

    protected static function view()
    {
        return self::$view_;
    }

    protected static function viewInit()
    {
        self::$view_ = View::Create();
    }
}
?>