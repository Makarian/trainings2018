#include "headers/gui/ChatMainWindow.hpp"

#include <QApplication>

ChatMainWindow::ChatMainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setupUi();
}

void
ChatMainWindow::setupUi()
{
    if (objectName().isEmpty()) {
        setObjectName(QStringLiteral("ChatMainWindow"));
    }
    resize(400, 300);

    menuBar_ = new QMenuBar(this);
    menuBar_->setObjectName(QStringLiteral("menuBar_"));
    setMenuBar(menuBar_);

    mainToolBar_ = new QToolBar(this);
    mainToolBar_->setObjectName(QStringLiteral("mainToolBar_"));
    addToolBar(mainToolBar_);

    centralWidget_ = new QWidget(this);
    centralWidget_->setObjectName(QStringLiteral("centralWidget_"));
    setCentralWidget(centralWidget_);

    statusBar_ = new QStatusBar(this);
    statusBar_->setObjectName(QStringLiteral("statusBar_"));
    setStatusBar(statusBar_);

    setWindowTitle(QApplication::translate("ChatMainWindow", "ChatMainWindow", Q_NULLPTR));

    QMetaObject::connectSlotsByName(this);
} // setupUi

ChatMainWindow::~ChatMainWindow()
{
}
