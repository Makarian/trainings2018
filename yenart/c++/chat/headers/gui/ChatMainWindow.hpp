#ifndef __CHAT_MAIN_WINDOW_HPP__
#define __CHAT_MAIN_WINDOW_HPP__

#include <QMainWindow>
#include <QMenuBar>
#include <QToolBar>
#include <QWidget>
#include <QStatusBar>

class ChatMainWindow : public QMainWindow
{
    //Q_OBJECT

public:
    explicit ChatMainWindow(QWidget *parent = 0);
    ~ChatMainWindow();
private:
    void setupUi();
private:
    QMenuBar* menuBar_;
    QToolBar* mainToolBar_;
    QWidget* centralWidget_;
    QStatusBar* statusBar_;
};

#endif /// __CHAT_MAIN_WINDOW_HPP__
