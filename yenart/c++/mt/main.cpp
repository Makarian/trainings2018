#include <iostream>
#include <thread>
#include <chrono>
#include <atomic>
#include <mutex>

class Timer
{
public:
    Timer(const std::string& message = "")
        : start_(std::chrono::system_clock::now())
        , message_(message)
        {
    }
    ~Timer() {
        std::chrono::_V2::system_clock::time_point end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsedSeconds = end - start_;
        std::cout << (message_.empty() ? "Elapsed seconds: " : message_);
        std::cout << elapsedSeconds.count() << std::endl;
    }
private:
    std::chrono::_V2::system_clock::time_point start_;
    std::string message_;
};

void
f(int i)
{
    Timer t;
    std::cout << "Hello from Thread "
              << i << "/" << std::thread::hardware_concurrency
              << std::endl;
}

const char* incrementCountStr = ::getenv("INCREMENT_COUNT");
const int incrementCount = incrementCountStr != NULL ? ::atoi(incrementCountStr) : 10000;
int n = 0;

void
increment()
{
    for (int i = 0; i < incrementCount; ++i) {
        ++n;
    }
}

std::atomic<int> na(0);
void
incrementAtomic()
{
    for (int i = 0; i < incrementCount; ++i) {
        ++na;
    }
}

class Locker
{
public:
    Locker(std::mutex& m) : mutex_(m) {
        mutex_.lock();
    }
    ~Locker() {
        mutex_.unlock();
    }
private:
    std::mutex& mutex_;
};

int nm = 0;
std::mutex m;
void
incrementMutex()
{
    for (int i = 0; i < incrementCount; ++i) {
        Locker l(m);
        ++nm;
    }
}

int
main()
{
    {
        Timer timer("Overall Shared Elapsed Time: ");
        const int THREAD_COUNT = 4;
        std::thread t[THREAD_COUNT];
        for (int i = 0; i < THREAD_COUNT; ++i) {
            t[i] = std::thread(increment);
        }
        for (int i = 0; i < THREAD_COUNT; ++i) {
            t[i].join();
        }
        std::cout << n << std::endl;
    }
    {
        Timer timer("Overall Atomic Elapsed Time: ");
        const int THREAD_COUNT = 4;
        std::thread t[THREAD_COUNT];
        for (int i = 0; i < THREAD_COUNT; ++i) {
            t[i] = std::thread(incrementAtomic);
        }
        for (int i = 0; i < THREAD_COUNT; ++i) {
            t[i].join();
        }
        std::cout << na << std::endl;
    }
    {
        Timer timer("Overall Mutex Elapsed Time: ");
        const int THREAD_COUNT = 4;
        std::thread t[THREAD_COUNT];
        for (int i = 0; i < THREAD_COUNT; ++i) {
            t[i] = std::thread(incrementMutex);
        }
        for (int i = 0; i < THREAD_COUNT; ++i) {
            t[i].join();
        }
        std::cout << nm << std::endl;
    }
    return 0;
}

