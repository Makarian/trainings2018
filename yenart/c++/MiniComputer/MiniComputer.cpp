#include "MiniComputer.hpp"

MiniComputer::MiniComputer()
{
}

MiniComputer::MiniComputer(std::string name)
{
    setComputerName(name);
}


std::string
MiniComputer::getComputerName()
{
    return computerName_;
}


void
MiniComputer::setComputerName(std::string name)
{
    if (name.length() > 25) {
        computerName_ = name.substr(0, 25);
        return;
    }

    computerName_ = name;
}

