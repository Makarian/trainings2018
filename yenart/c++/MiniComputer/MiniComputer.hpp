#include <string>

class MiniComputer
{
public:
    MiniComputer();
    MiniComputer(std::string name);
    std::string getComputerName();
    void setComputerName(std::string name);
private:
    std::string computerName_;
};

