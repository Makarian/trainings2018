#include <iostream>

class Object
{
public:
    int getSomething()    const { return 0; }
    int getSomething()          { return 1; }
    int getNonConstSomething()  { return 0; }
};

int
main()
{
    Object object;
    std::cout << object.getSomething() << std::endl;
    return 0;
}

