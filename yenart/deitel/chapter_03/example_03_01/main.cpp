#include <iostream>

class Layer
{
public:
    void
    show()
    {
        std::cout << "Hardcoded Layer!" << std::endl;
    }
};

int
main()
{
    Layer layer;
    layer.show();
    return 0;
}

