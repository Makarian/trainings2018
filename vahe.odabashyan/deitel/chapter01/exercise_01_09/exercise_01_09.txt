Give a brief answer to each of the following questions: 
1. Why does this text discuss structured programming in addition to object-oriented programming?
 
The text represents the improvements in the creation of software. Mainly, it discusses the OOP benefits over structured programming. Unlike OOP, the developers were obliged to create every project from scratch when using structured programming. This was because the program units were not reusable, hence software creation was time-consuming and expensive. In case OOP we have reusable components mirroring real-world objects which enable us to mitigate all the problems of the structured programming.

2.What are the typical steps (mentioned in the text) of an object-oriented design process?

1) Analyzing the requirements of a software system to be built. => 2) Determination of the needed objects for that system => 3) Determination of the needed attributes of the objects => 4) Determination of the objects' behaviors => 5) Specification of the objects interaction scheme.


3.What kinds of messages do people send to one another?

Written messages, body language, voice messages, etc.


4. Objects send messages to one another across well-defined interfaces. What interfaces does a car radio (object) present to its user (a person object)?

All the buttons that the car radio has.

