import sys;
import cv2;

def openVideo(fileName):
    video = cv2.VideoCapture(fileName);
    while video.isOpened():
        ret, frame = video.read();
        if ret != True:
            break;
        cv2.imshow(fileName + "\t(Quit with 'q')", frame);
        if cv2.waitKey(25) & 0xFF == ord('q'): ### 25 frame/second (PAL)
            break;
    video.release();
    cv2.destroyAllWindows();

def usage():
    print("Usage");
    print("\tpython video.py <filename>");

def main():
    if len(sys.argv) < 2:
        usage();
        return;

    fileName = sys.argv[1];
    openVideo(fileName);

if __name__ == "__main__":
    main();

