#include <iostream>
#include <unistd.h>

int
main()
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter sales in dollars (-1 to end): ";
        }
        double salesInDollars;
        std::cin >> salesInDollars;
        if (-1 == salesInDollars) {
            break;
        }
        if (salesInDollars < 0) {
            std::cerr << "Error 1: Wrong input of the sales in dollars!" << std::endl;
            return 1;
        }

        std::cout << "Salary is: $" << 200 + salesInDollars * 0.09 << std::endl << std::endl;
    }

    return 0;
}

