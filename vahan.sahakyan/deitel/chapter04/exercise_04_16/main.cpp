#include <iostream>
#include <unistd.h>
#include <iomanip>

int
main()
{
    while (true) {
/// HOURS WORKED
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter hours worked (-1 to end): ";
        }
        int hours;
        std::cin >> hours;
        if (-1 == hours) { /// Sentinel controlled exit
            break;
        }
        if (hours < 0) {
            std::cerr << "Error 1: Hours can't be negative." << std::endl;
            return 1;
        }
/// HOURLY RATE
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter hourly rate of the worker ($00.00): ";
        }
        double hourlyRate;
        std::cin >> hourlyRate;
        if (hourlyRate <= 0) {
            std::cerr << "Error 2: Hourly rate must be higher than Zero!" << std::endl;
            return 2;
        }
/// SALARY COUNTER
        double salary = hourlyRate * (hours > 40 ? hours * 1.5 - 20 : hours);
/// PROGRAM OUTPUT
        std::cout << "Salary is $" << std::setprecision(2) << std::fixed << salary << std::endl << std::endl;
    }

    return 0;
}
