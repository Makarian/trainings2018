a.---------------------------
x = 5, y = 8

Expected OUTPUT:
@@@@@
$$$$$
&&&&&

Solution for a:
if (y == 8) {
    if (x == 5) {
        cout << "@@@@@" << endl;
    }
    else {
        cout << "#####" << endl;
    }
    cout << "$$$$$" << endl;
    cout << "&&&&&" << endl;
}

b.---------------------------
x = 5, y = 8

Expected OUTPUT:
@@@@@

Solution for b:
if (y == 8) {
    if (x == 5) {
        cout << "@@@@@" << endl;
    }
    else {
        cout << "#####" << endl;
        cout << "$$$$$" << endl;
        cout << "&&&&&" << endl;
    }
}

c.---------------------------
x = 5, y = 8

Expected OUTPUT:
@@@@@
&&&&&

Solution for c:
if (y == 8) {
    if (x == 5) {
        cout << "@@@@@" << endl;
    }
    else {
        cout << "#####" << endl;
        cout << "$$$$$" << endl;
    }
    cout << "&&&&&" << endl;
}

d.---------------------------
x = 5, y = 7

Expected OUTPUT:
#####
$$$$$
&&&&&

Solution for d:
if (y == 8) {
    if (x == 5) {
        cout << "@@@@@" << endl;
    }
}
else {
    cout << "#####" << endl;
    cout << "$$$$$" << endl;
    cout << "&&&&&" << endl;
}
-----------------------------

