2.13 Which of the following C++ statements contain variables whose values are replaced?

a. cin >> b >> c >> d >> e >> f;
b. p = i + j + k + 7;
c. cout << "variables whose values are replaced";
d. cout << "a = 5";


Answers:


a. True: Inputing a value from the outside (i.e. by the User)
b. True: Changing the value after declaration
c. False: Text output
d. False: Text output
